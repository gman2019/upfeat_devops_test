import time
import redis
from flask import Flask, render_template, escape, redirect, url_for, session, request, jsonify
#import oauth2
from flask_oauthlib.client import OAuth

app = Flask(__name__)
app.config['GOOGLE_ID'] = "google_id"
app.config['GOOGLE_SECRET'] = "google_secret"
app.debug = True
app.secret_key = 'development'
oauth = OAuth(app)

google = oauth.remote_app(
    'google',
    consumer_key = app.config.get('GOOGLE_ID'),
    consumer_secret = app.config.get('GOOGLE_SECRET'),
    request_token_params = {'scope': 'email'},
    base_url = 'https://www.googleapis.com/oauth2/v1/',
    request_token_url = None,
    access_token_method='POST',
    access_token_url='https://accounts.google.com/o/oauth2/token',
    authorize_url='https://accounts.google.com/o/oauth2/auth',
)

@app.route('/')
def index():
    login = None
    image = None
    if 'google_token' in session:
        me = google.get('userinfo')
        if not 'error' in me.data.keys():
            login = me.data['email']
            image = me.data['picture']
    return render_template('index.html', login = login, image = image)

@app.route('/login')
def login():
    return google.authorize(callback=url_for('authorized', _external=True))


@app.route('/logout')
def logout():
    session.pop('google_token', None)
    return redirect(url_for('index'))


@app.route('/login/authorized')
def authorized():
    resp = google.authorized_response()
    if resp is None:
        return 'Access denied: reason=%s error=%s' % (
            request.args['error_reason'],
            request.args['error_description']
        )
    session['google_token'] = (resp['access_token'], '')
    me = google.get('userinfo')
    return redirect(url_for('index'))


@google.tokengetter
def get_google_oauth_token():
    return session.get('google_token')

#cache = redis.Redis(host='redis', port=6379)

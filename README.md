Upfeat test application

Author: Al Gortemaker

Public site: http://upfeattest.tk/

Tasks
• Create a public GIT repo (Github / Gitlab / Bitbucket) (complete)
• Create a basic hello world in a framework of choice (Express.JS / Laravel / Flask / etc) (complete)
• Create a Docker-compose.yml to provision your environment of choice (so the application can be viewed/edited locally) (complete)
• Implement user registration (or social-backed login) (social-backed login complete)
• Write an ansible script to deploy the application to a free plan on an IaaS provider (Heroku / AWS/ GCP / etc) (in progress)
• Craft a scheduled task (using ansible) on your cloud provider to perform backups of your app database that are encrypted and stored in the git repo (hourly) (incomplete)
• Create a free development domain (.tk or alternative) (complete)
• Create a free CDN account configure your domain of choice; and script the basic setup of a full-page-cache (incomplete)
• Provision DNS for the domain so a "https://www.domain.tld" record points to the application (complete, though no TLS)
• Include the app URL in the README.MD in your local repo (complete)
• Publish all of the above to your public repo (complete)

Deliverables:
• Submit the URL to the public repo
• Submit the encryption key + any steps to decrypt (if not obvious) for the database
dumps

Expected outcome:
• The URL should be publicly accessible
• The total time to load the page should be <3 seconds (first load) and <1 second (repeat
loads)
• Newly signed up users should appear in the encrypted dump after the scheduled period

